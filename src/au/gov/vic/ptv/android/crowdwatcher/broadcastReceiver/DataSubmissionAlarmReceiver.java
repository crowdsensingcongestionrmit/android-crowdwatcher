package au.gov.vic.ptv.android.crowdwatcher.broadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import au.gov.vic.ptv.android.crowdwatcher.model.DataSubmission.DataSubmissionIntentService;

/**
* This class is used by AlarmManager to fire service intent to submit data
* to web server
*
*/
public class DataSubmissionAlarmReceiver extends BroadcastReceiver {

    public void onReceive(Context c, Intent intent) {
        Log.d("DataSubmissionAlarmReceiver", "onReceive");
        Intent newIntent = new Intent(c,DataSubmissionIntentService.class);
        Toast.makeText(c, "DataSubmissionAlarmReceiver starts!!!!.",
                Toast.LENGTH_LONG).show();
        c.startService(newIntent);
    }
}
