package au.gov.vic.ptv.android.crowdwatcher.broadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import au.gov.vic.ptv.android.crowdwatcher.model.dataCollection.LocationService;

/**
 * This class is used disable LocationService polling location data from background
 */
public class LocationDisabledBroadcastReceiver extends BroadcastReceiver{

    private LocationService locationService;

    public LocationDisabledBroadcastReceiver(){}

    public LocationDisabledBroadcastReceiver(LocationService service) {
        this.locationService = service;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        locationService.disableBluetooth();
    }
}
