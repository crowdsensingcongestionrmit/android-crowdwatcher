package au.gov.vic.ptv.android.crowdwatcher.broadcastReceiver;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import au.gov.vic.ptv.android.crowdwatcher.model.dataCollection.GetNearbyStopServiceAsyncTask;
import au.gov.vic.ptv.android.crowdwatcher.model.dataCollection.LocationService;
import au.gov.vic.ptv.android.crowdwatcher.R;
import au.gov.vic.ptv.android.crowdwatcher.model.SensorsDatabase;

/**
 * This class is used for receiving Location to check for confidence level if the device is in
 * the radius of the station.
 *
 */
public class LocationUpdateBroadcastReceiver extends BroadcastReceiver {

    private Location location;
    private LocationService service;
    private SensorsDatabase sensorsDatabase;

    public LocationUpdateBroadcastReceiver(){}

    /**
     * Constructs an object that can be used to start and stop sensor collection.
     * <p>
     *
     * @param service  the LocationService which is used to retrieve GPS data.
     *
     * @param sensorsDatabase  the database object used to store the station data. If the context is null,
     *            the sensor object which uses the database instance will crash the application when
     *            trying to store the sensor data. All subclass of CollectSensor shall use the same
     *            database instance to store the sensor data.
     *
     */
    public LocationUpdateBroadcastReceiver(LocationService service, SensorsDatabase sensorsDatabase) {
        this.sensorsDatabase = sensorsDatabase;
        this.service = service;
    }

    /**
     * Calculates the confidence level to check if the device is in the station. Stores the station
     * in database and enable bluetooth in discovery mode if device is in the station.
     * @param context context of the broadcast
     * @param intent intent of the locationObject received from LocationService
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        boolean inStop = false;

        location = intent.getParcelableExtra("locationObject");
        GetNearbyStopServiceAsyncTask getNearbyStopServiceAsyncTask = new GetNearbyStopServiceAsyncTask(service, location);
        try {
            ArrayList<HashMap<String, String>> nearbyStops = getNearbyStopServiceAsyncTask.execute().get();

            for (HashMap<String, String> stop : nearbyStops) {
                if (Double.parseDouble(stop.get(context.getString(R.string.nearby_stop_confidence_level_tag))) > 80) {
                    if (!service.isBluetoothEnabled()){
                        service.enableBluetooth();
                    }
                    inStop = true;
                    sensorsDatabase.addStationRecord(stop.get(context.getString(R.string.nearby_stop_id_tag)),System.currentTimeMillis());
                    break;
                }
            }

            // stops bluetooth scanning if device is not in station/stop
            if(!inStop){
                service.disableBluetooth();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
