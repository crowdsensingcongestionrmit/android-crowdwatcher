package au.gov.vic.ptv.android.crowdwatcher;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import au.gov.vic.ptv.android.crowdwatcher.JSONParser.JSONParser;
import au.gov.vic.ptv.android.crowdwatcher.model.calculator.ConfidenceLevelCalculator;
import au.gov.vic.ptv.android.crowdwatcher.model.PTVAPIsignature;
import au.gov.vic.ptv.android.crowdwatcher.sensorcollection.pushsensor.LocationSensor;

/**
 * This class is only used to get nearby stop when the refresh button is clicked. This
 * is used for prototype purpose.
 */
public class GetNearbyStopAsyncTask extends AsyncTask<String, Void, ArrayList<HashMap<String,String>>> {

//    private NearbyStopListViewAdapter adapter;
	private Context context;
//    private ProgressDialog progressDialog;
    private LocationSensor locationSensor;
    private final HttpClient Client = new DefaultHttpClient();
    private String Content;
    private String Error = null;
    private String data ="";
    private int sizeData = 0; 
    private final int stopsDistancetreshold = 500;
    private int stopRadius = 55;

    public GetNearbyStopAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        progressDialog = new ProgressDialog(context);
//
//        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                if(locationSensor != null)
//                    locationSensor.unregister();
//                progressDialog.dismiss();
//                cancel(true);
//            }
//        });


        this.locationSensor = new LocationSensor(context,null);
        if(!locationSensor.isProviderEnabled()) {
            Toast.makeText(context, "Please neable location Service on phone", Toast.LENGTH_LONG).show();
            cancel(true);
            return;
        }
        locationSensor.register();
        Log.i("check","running asyncTask");
//        progressDialog.setMessage("Getting GPS Location...");
//        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        progressDialog.setProgress(1);
//        progressDialog.show();
    }


    @Override
    protected ArrayList<HashMap<String, String>> doInBackground(String... params) {
        Log.d("GetNearbyStopAsyncTask", "doInBackground start");

        Calendar t = Calendar.getInstance();
        long startTime = t.getTimeInMillis();

        while(locationSensor.getCurrentLocation() == null && Calendar.getInstance().getTimeInMillis() - startTime < 15000){
            try {
                Log.d("GetNearbyStopAsyncTask", "waiting GPS "+locationSensor.getCurrentLocation());
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Location location = locationSensor.getCurrentLocation();
        locationSensor.unregister();
        if(location == null) {
            Log.d("GetNearbyStopAsyncTask", "Unable to retrieve you current Location");
        }
        else {
            Log.d("GetNearbyStopAsyncTask", "Long :" + location.getLongitude() + " Lat:" + location.getLatitude() + " Acc:" + location.getAccuracy()+" Provider:"+location.getProvider());
        }


        ArrayList<HashMap<String,String>> nearbyStops;
        JSONParser jsonParser = new JSONParser(context);
        String APIResponse =requestPTVAPI(location.getLatitude(), location.getLongitude());
        try {
        	nearbyStops = jsonParser.parseNearByStation(APIResponse);
        } catch (JSONException e) {
            e.printStackTrace();
            nearbyStops =  new ArrayList<HashMap<String, String>>();
        }
        
        nearbyStops = filterList(nearbyStops);

        return nearbyStops;
    }

    @Override
    protected void onPostExecute(ArrayList<HashMap<String, String>> nearbyStops) {
//        progressDialog.dismiss();
        ArrayList<HashMap<String, String>>filterStops = filterList(nearbyStops);
//        adapter.updateListData(filterStops);
//        adapter.notifyDataSetChanged();
    }

    private ArrayList<HashMap<String, String>> filterList(
			ArrayList<HashMap<String, String>> nearbyStops) {
		// TODO Auto-generated method stub
    	ConfidenceLevelCalculator calculator = new ConfidenceLevelCalculator();
    	/* (1)filter the nearbyStops, create a new collection(eg ArrayList),
    	 * 	  that has all stops distance less than stopsDistancetreshold(It is instantiate at the top).*/

        ArrayList<HashMap<String,String>> nearbyStopWithinTreshold = new ArrayList<HashMap<String, String>>();

        for(HashMap<String,String> stop : nearbyStops){
           double distance = Double.parseDouble(stop.get(context.getString(R.string.nearby_stop_distance_tag)));
            if(distance < stopsDistancetreshold){
                 /* (2)for all stops in the new collection, call the calculateOverlapArea in calculator, and add the return value
                 *    into the stops detail(the method return the percentage area of overlap of two circle)
                 *    this method receives(GPS Accuracy, stopRadius, stop distance)
                 *    stopRadius is defined at top.*/
                double confidenceLevel = calculator.calculateConfidenceLevel(locationSensor.getCurrentLocation().getAccuracy(),stopRadius,distance);
                stop.put(context.getString(R.string.nearby_stop_confidence_level_tag),String.valueOf(confidenceLevel));
                nearbyStopWithinTreshold.add(stop);
            }
        }
        /* (3)return the processed collection */
		return nearbyStopWithinTreshold;
	}

	private String requestPTVAPI(double lat, double longi) {
    	// TODO Auto-generated method stub
    	String responseString ="";
    	HttpURLConnection urlConnection = null;
    	PTVAPIsignature instance = PTVAPIsignature.getInstance();
    	String temp = instance.generateStopsNearbyURL(lat, longi);
    	String url = instance.generateCompleteURLWithSignature(instance.getPrivateKey(), temp, Integer.parseInt(instance.getDevID()));
    	try 
    	{  

    		// Defined URL  where to send data 
    		URL urlWithSignature = new URL(url); 

    		urlConnection = (HttpURLConnection) urlWithSignature.openConnection();
    		int responseCode = urlConnection.getResponseCode();

    		if(responseCode == HttpStatus.SC_OK){
    			responseString = readStream(urlConnection.getInputStream());
    			Log.v("CatalogClient", responseString);
    		}else{
    			Log.v("CatalogClient", "Response code:"+ responseCode);
    		}

    	} catch (Exception e) {
    		e.printStackTrace();
    	} finally {
    		if(urlConnection != null)
    			urlConnection.disconnect();
    	}

    	return responseString;
    }
    
    private String readStream(InputStream in) {
		  BufferedReader reader = null;
		  StringBuffer response = new StringBuffer();
		  try {
		    reader = new BufferedReader(new InputStreamReader(in));
		    String line = "";
		    while ((line = reader.readLine()) != null) {
		      response.append(line);
		    }
		  } catch (IOException e) {
		    e.printStackTrace();
		  } finally {
		    if (reader != null) {
		      try {
		        reader.close();
		      } catch (IOException e) {
		        e.printStackTrace();
		      }
		    }
		  }
		  return response.toString();
		}
}
