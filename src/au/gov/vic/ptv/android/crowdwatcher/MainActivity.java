package au.gov.vic.ptv.android.crowdwatcher;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import au.gov.vic.ptv.android.crowdwatcher.broadcastReceiver.DataSubmissionAlarmReceiver;
import au.gov.vic.ptv.android.crowdwatcher.model.SensorsDatabase;
import au.gov.vic.ptv.android.crowdwatcher.model.dataCollection.LocationService;


public class MainActivity extends Activity {

//    private ListView nearbyStopListView;
//    private NearbyStopListViewAdapter listAdapter;
	
	private SensorsDatabase sensorsDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        nearbyStopListView = (ListView)findViewById(R.id.nearbyStopListView);
//        listAdapter = new NearbyStopListViewAdapter(this,R.layout.listviewrow, new ArrayList<HashMap<String, String>>());
//        nearbyStopListView.setAdapter(listAdapter);
        sensorsDatabase = new SensorsDatabase(this);
        String key = retrieveDeviceKey();
        if(key != null){
        	TextView deviceKey = (TextView) findViewById(R.id.deviceKey); 
        	deviceKey.setText(key);
        }
        scheduleDataSubmission();
        startService(new Intent(this, LocationService.class));
        updateGPS();
    }
    
    protected void updateGPS() {
		// TODO Auto-generated method stub
		GetNearbyStopAsyncTask updateTask = new GetNearbyStopAsyncTask(this.getApplicationContext());
        updateTask.execute("URL");
	}

    @Override
    protected void onPause() {
        super.onPause();
        sensorsDatabase.close();
    }

//    public void refreshList(View view){
//        GetNearbyStopAsyncTask updateTask = new GetNearbyStopAsyncTask(listAdapter);
//        updateTask.execute("URL");
//    }

    /*
     * Schedules AlarmManager to submit data to web server every x intervals
     *
     */
    private void scheduleDataSubmission(){

        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this,DataSubmissionAlarmReceiver.class);

        // checks if alarm has already been scheduled
        boolean alarmUp = (PendingIntent.getBroadcast(this.getApplicationContext(), 1234567, intent, PendingIntent.FLAG_NO_CREATE) != null);
        if(alarmUp){
            Log.d("scheduleDataSubmission", "Alarm for submission have been scheduled!!");
            return;
        }

        Log.d("scheduleDataSubmission", "schedule dataSubmission");
        //create a pending intent to be called
        PendingIntent  dataSubmissionPI= PendingIntent.getBroadcast(this.getApplicationContext(), 1234567, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        //schedule time for pending intent, and set the interval to 30 seconds
        alarm.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), 1*30*1000, dataSubmissionPI);
    }

    // returns null if device key is not retrieved from web server
    private String retrieveDeviceKey(){
        // get device key from sharedPreference
        SharedPreferences pref;
        pref = getSharedPreferences("au.gov.vic.ptv.android.crowdwatcher", MODE_MULTI_PROCESS);
        String deviceKey = pref.getString("au.gov.vic.ptv.android.crowdwatcher.key", null);

        return deviceKey;
    }
    
    public void walking(View view){
    	updateGPS();
    	sensorsDatabase.addEventRecord("walking", System.currentTimeMillis());
    }
    
    public void standing(View view){
    	updateGPS();
    	sensorsDatabase.addEventRecord("standing", System.currentTimeMillis());
    }
    
    public void gettingOnTram(View view){
    	updateGPS();
    	sensorsDatabase.addEventRecord("getting on tram", System.currentTimeMillis());
    }

    public void gettingOffTram(View view){
    	updateGPS();
    	sensorsDatabase.addEventRecord("getting off tram", System.currentTimeMillis());
    }
    
    public void tramMoving(View view){
    	updateGPS();
    	sensorsDatabase.addEventRecord("tram moving", System.currentTimeMillis());
    }
    
    public void tramStopped(View view){
    	updateGPS();
    	sensorsDatabase.addEventRecord("tram stopped", System.currentTimeMillis());
    }
    
    public void gettingOnTrain(View view){
    	updateGPS();
    	sensorsDatabase.addEventRecord("getting on train", System.currentTimeMillis());
    }
    
    public void gettingOffTrain(View view){
    	updateGPS();
    	sensorsDatabase.addEventRecord("getting off train", System.currentTimeMillis());
    }
    
    public void trainMoving(View view){
    	updateGPS();
    	sensorsDatabase.addEventRecord("train moving", System.currentTimeMillis());
    }
    
    public void waitingTram(View view){
    	updateGPS();
    	sensorsDatabase.addEventRecord("waiting tram", System.currentTimeMillis());
    }
    
    public void trainStopped(View view){
    	updateGPS();
    	sensorsDatabase.addEventRecord("train stopped", System.currentTimeMillis());
    }
    
    public void waitingTrain(View view){
    	updateGPS();
    	sensorsDatabase.addEventRecord("waiting train", System.currentTimeMillis());
    }
    
    public void doorOpened(View view){
    	updateGPS();
    	sensorsDatabase.addEventRecord("door opened", System.currentTimeMillis());
    }
    
    public void doorClosed(View view){
    	updateGPS();
    	sensorsDatabase.addEventRecord("door closed", System.currentTimeMillis());
    }
}
