package au.gov.vic.ptv.android.crowdwatcher.sensorcollection;

/**
 * This class should be extended by Push/Pull Sensor class.
 *
 * This class provides generic method for registering and unregistering sensor for collecting
 * sensor data.
 */
public abstract class CollectSensor {

    /**
     * Starts sensor collection
     */
    public abstract void register();

    /**
     * Stops sensor sensor collection
     */
    public abstract void unregister();
}
