package au.gov.vic.ptv.android.crowdwatcher.sensorcollection.pushsensor;


import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import au.gov.vic.ptv.android.crowdwatcher.model.SensorsDatabase;


/**
 * This class is used for collecting LocationSensor data sensor data.
 * It extends the PushSensor to starts and stops LocationSensor Sensor
 * collection. The LocationSensor will update location for every 1 s and 0m travelled.
 *
 */
public class LocationSensor extends PushSensor {

	private LocationManager locationManager;
    private static final int THRESHOLD = 1000 * 10 * 1;
	//will update location every 1 s and 0m travelled
	private int MIN_UPDATE_TIME = 1000;
	private int MIN_UPDATE_DIST = 0;

    private boolean gpsEnabled = false;
    private boolean networkEnabled = false;
    Location currentBestLocation;

	// Used as unique name space for broadcasting intent
	public static final String ACTION_LOCATION_BROADCAST = "au.gov.vic.ptv.android.crowdwatcher.broadcastReceiver.LocationUpdateBroadcastReceiver.update";

    /**
     * Constructs an object that can be used to start and stop sensor collection.
     * <p>
     *
     * @param context  the context used to create and start the sensor collection.
     *                 If the context is null, the object constructed cannot be used to
     *                 start collection.
     *
     * @param db  the database object used to store the sensor data. If the context is null,
     *            the sensor object which uses the database instance will crash the application when
     *            trying to store the sensor data. All subclass of CollectSensor shall use the same
     *            database instance to store the sensor data.
     *
     */
	public LocationSensor(Context context, SensorsDatabase db) {
        super(context,db);
        locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
	}

    /**
     * create LocationListener to be used by this service DataCollectionService
     * to update location. Adds location data to database when update is received.
     */
	private LocationListener locationListener = new LocationListener() {

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
            if(provider.equals(LocationManager.GPS_PROVIDER)){

                switch(status) {
                    case LocationProvider.AVAILABLE:
                        //gpsEnabled = true;
                        break;
                    case LocationProvider.OUT_OF_SERVICE:
                        //gpsEnabled = false;
                        break;
                    case LocationProvider.TEMPORARILY_UNAVAILABLE:
                        break;
                }

            } else if (provider.equals(LocationManager.NETWORK_PROVIDER)){
                networkEnabled = false;
                switch(status) {
                    case LocationProvider.AVAILABLE:
                        //networkEnabled = true;
                        break;
                    case LocationProvider.OUT_OF_SERVICE:
                        //networkEnabled = false;
                        break;
                    case LocationProvider.TEMPORARILY_UNAVAILABLE:
                        break;
                }
            }

		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
            if(provider.equals(LocationManager.GPS_PROVIDER)){
                gpsEnabled = true;
            } else if (provider.equals(LocationManager.NETWORK_PROVIDER)){
                networkEnabled = true;
            }
		}

		@Override
		public void onProviderDisabled(String provider) {
            if(provider.equals(LocationManager.GPS_PROVIDER)){
                gpsEnabled = false;
                if(!gpsEnabled && !networkEnabled){
                    Intent intent = new Intent("au.gov.vic.ptv.android.crowdwatcher.broadcastReceiver.LocationUpdateBroadcastReceiver.locationDisable");
                    LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
                }
            } else if (provider.equals(LocationManager.NETWORK_PROVIDER)){
                networkEnabled = false;
                if(!gpsEnabled && !networkEnabled){
                    Intent intent = new Intent("au.gov.vic.ptv.android.crowdwatcher.broadcastReceiver.LocationUpdateBroadcastReceiver.locationDisable");
                    LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
                }
            }

		}

		@Override
		public void onLocationChanged(Location location) {
			// TODO Auto-generated method stub
//            Log.d("LocationSensor","onLocationChanged"+location);
            if(isBetterLocation(location,currentBestLocation)){
                currentBestLocation = location;
//                Log.d("LocationSensor","onLocationChanged currentBestLocation"+currentBestLocation);
            }
            if(currentBestLocation==null)
            	return;
            SensorsDatabase db = getDb();
            if(db==null){
            	db = new SensorsDatabase(context);
            }
            db.addGPSReading(currentBestLocation.getLatitude(),currentBestLocation.getLongitude(),currentBestLocation.getAccuracy(),currentBestLocation.getTime());
			sendBroadcastMessage(currentBestLocation);


        }
	};

    /**
     * Starts the Location data collection and data collected will be stored in database.
     * The sensor will store location longitude, latitude, accuracy, location data scan time.
     * The sensor will try to get location from NETWORK_PROVIDER, GPS_PROVIDER and PASSIVE_PROVIDER.
     */
    public void register(){
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_UPDATE_TIME,MIN_UPDATE_DIST, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_UPDATE_TIME,MIN_UPDATE_DIST, locationListener);
        locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, MIN_UPDATE_TIME,MIN_UPDATE_DIST, locationListener);
    }

    /**
     * Stops the Location data collection.
     */
    public void unregister(){
        locationManager.removeUpdates(locationListener);
    }

    public Location getCurrentLocation(){
        return currentBestLocation;
    }

    /**
     * Broadcast Location to LocationUpdateBroadcastReceiver to calculate confidence level
     */
	private void sendBroadcastMessage(Location location){
		if(location !=null){
			//populates intent
			Intent intent = new Intent(ACTION_LOCATION_BROADCAST);
            intent.putExtra("locationObject",location);
			LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
		}
	}

    public boolean isProviderEnabled(){
        if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) &&
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) &&
                locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER))
            return true;
        else
            return false;
    }

    /** Determines whether one Location reading is better than the current Location fix
     * @param location  The new Location that you want to evaluate
     * @param currentBestLocation  The current Location fix, to which you want to compare the new one
     */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > THRESHOLD;
        boolean isSignificantlyOlder = timeDelta < - THRESHOLD;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }
}
