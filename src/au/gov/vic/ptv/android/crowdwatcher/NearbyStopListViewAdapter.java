package au.gov.vic.ptv.android.crowdwatcher;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class NearbyStopListViewAdapter extends ArrayAdapter {

    private final Context context;
    private ArrayList<HashMap<String,String>> values;

    public NearbyStopListViewAdapter(Context context, int resource, ArrayList<HashMap<String,String>>values) {
        super(context, resource, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        NumberFormat formatter = new DecimalFormat("##.##"); 

        View viewRow = inflater.inflate(R.layout.listviewrow,parent,false);
        TextView stopId = (TextView) viewRow.findViewById(R.id.stopName);
        TextView stopDistance = (TextView) viewRow.findViewById(R.id.stopDistance);
        TextView transportType = (TextView) viewRow.findViewById(R.id.transportType);
        TextView confidenceLevel = (TextView) viewRow.findViewById(R.id.confidenceLevel);

        HashMap<String,String> stopDetails = values.get(position);
        stopId.setText("Stop Id: "+stopDetails.get(context.getString(R.string.nearby_stop_id_tag)));
        stopDistance.setText("Distance: "+formatter.format(Double.parseDouble(stopDetails.get(context.getString(R.string.nearby_stop_distance_tag)))));
        transportType.setText("Transport Type: "+stopDetails.get(context.getString(R.string.nearby_stop_transport_type_tag)));

        confidenceLevel.setText("Confidence Level: "+formatter.format(Double.parseDouble(stopDetails.get(context.getString(R.string.nearby_stop_confidence_level_tag))))+"%");

        return viewRow; 
    }

    public void updateListData(ArrayList<HashMap<String,String>> updateValues){
        this.values.clear();
        this.values.addAll(updateValues);
    }

    public ArrayList<HashMap<String,String>> getNearbyStops(){
        return this.values;
    }
}
