package au.gov.vic.ptv.android.crowdwatcher.JSONParser;


import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import au.gov.vic.ptv.android.crowdwatcher.R;

public class JSONParser {

    private Context context;

    public JSONParser(Context context) {
        this.context = context;
    }

    public boolean parseHealthCheck(String healthCheckResponse) throws JSONException {
        JSONObject health = new JSONObject(healthCheckResponse);

        // indicates whether your key is valid/signature is calculated correctly
        boolean securityTokenOK = health.getBoolean("securityTokenOK");
        // indicates whether your clock is synchronised with our clock within 3 minutes
        boolean clientClockOK = health.getBoolean("clientClockOK");
        // indicates status of the performance cache
        boolean memcacheOK = health.getBoolean("memcacheOK");
        // indicates availability of the data
        boolean databaseOK = health.getBoolean("databaseOK");

        return (clientClockOK && databaseOK && memcacheOK && securityTokenOK);
    }

    public ArrayList<HashMap<String,String>> parseNearByStation(String nearbyStationResponse) throws JSONException {

        ArrayList<HashMap<String,String>> nearbyStopsList = new ArrayList();
        JSONArray nearbyStationResults = new JSONArray(nearbyStationResponse);

        for(int i =0; i < nearbyStationResults.length();i++){
            JSONObject result = nearbyStationResults.getJSONObject(i).getJSONObject("result");
            Log.d("result",result.toString(4));
            HashMap<String,String> nearbyStop = new HashMap<String, String>();

            nearbyStop.put(context.getString(R.string.nearby_stop_transport_type_tag),
                    result.getString(context.getString(R.string.nearby_stop_transport_type_tag)));

            nearbyStop.put(context.getString(R.string.nearby_stop_id_tag),
                    String.valueOf(result.getString(context.getString(R.string.nearby_stop_id_tag))));

            nearbyStop.put(context.getString(R.string.nearby_stop_distance_tag),
                    String.valueOf(result.getDouble(context.getString(R.string.nearby_stop_distance_tag))*100000000));

            nearbyStopsList.add(nearbyStop);
        }

        return nearbyStopsList;
    }
}
