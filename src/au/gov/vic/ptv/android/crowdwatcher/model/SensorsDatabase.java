package au.gov.vic.ptv.android.crowdwatcher.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import au.gov.vic.ptv.android.crowdwatcher.model.sensorReading.BluetoothReading;
import au.gov.vic.ptv.android.crowdwatcher.model.sensorReading.CellTowerReading;
import au.gov.vic.ptv.android.crowdwatcher.model.sensorReading.EventRecord;
import au.gov.vic.ptv.android.crowdwatcher.model.sensorReading.GPSReading;
import au.gov.vic.ptv.android.crowdwatcher.model.sensorReading.StationRecord;

public class SensorsDatabase extends SQLiteOpenHelper{
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "SensorsDB";
	
	private static final String TABLE_GPS = "gps";
    private static final String TABLE_BLUETOOTH = "bluetooths";
    private static final String TABLE_CELLTOWER = "celltower";
    private static final String TABLE_STATION = "station";
    private static final String TABLE_EVENT = "event";
    
    private static final String GPS_LONGITUDE = "longitude";
    private static final String GPS_LATITUDE = "latitude";
    private static final String GPS_ACCURACY = "accuracy";
    private static final String GPS_TIME = "time";
    
    private static final String BLUETOOTH_ID = "id";
    private static final String BLUETOOTH_TIME = "time";
    private static final String BLUETOOTH_RSSI = "rssi";
    private static final String BLUETOOTH_TYPE = "isletype";
	
    private static final String CELL_MCC = "mcc";
    private static final String CELL_MNC = "mnc";
    private static final String CELL_ID = "id";
    private static final String CELL_LAC = "lac";
    private static final String CELL_TIME = "time";
    
    private static final String STATION_TIME = "time";
    private static final String STATION_ID = "id";
    
    private static final String EVENT_TIME = "time";
    private static final String EVENT_NAME = "name";
    
    
	public SensorsDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);  
    }
	
	public void onCreate(SQLiteDatabase db) {
        String CREATE_GPS_TABLE = "CREATE TABLE gps ( " +
                "longitude DOUBLE, "+
                "latitude DOUBLE, "+
                "accuracy DOUBLE, "+
                "time INTEGER )";
    	
    	db.execSQL(CREATE_GPS_TABLE);
    	
    	String CREATE_BLUETOOTH_TABLE = "CREATE TABLE bluetooths ( " +
                "id TEXT, "+
                "rssi INTEGER, "+
                "isletype INTEGER, "+
                "time INTEGER )";
    	
    	db.execSQL(CREATE_BLUETOOTH_TABLE);
    	
    	String CREATE_CELLTOWER_TABLE = "CREATE TABLE celltower ( " +
                "id INTEGER, "+
                "mcc INTEGER, "+
                "time INTEGER, "+
                "mnc INTEGER, "+
                "lac INTEGER )";
    	
    	db.execSQL(CREATE_CELLTOWER_TABLE);
    	
    	String CREATE_STATION_TABLE = "CREATE TABLE station ( " +
                "id TEXT, "+
                "time INTEGER )";
    	
    	db.execSQL(CREATE_STATION_TABLE);
    	
    	String CREATE_EVENT_TABLE = "CREATE TABLE event ( " +
                "name TEXT, "+
                "time INTEGER )";
    	
    	db.execSQL(CREATE_EVENT_TABLE);
	}
	
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older books table if existed
        db.execSQL("DROP TABLE IF EXISTS gps");
        db.execSQL("DROP TABLE IF EXISTS bluetooths");
        db.execSQL("DROP TABLE IF EXISTS celltower");
        db.execSQL("DROP TABLE IF EXISTS station");
        db.execSQL("DROP TABLE IF EXISTS event");
        this.onCreate(db);
	}
	
	public void addGPSReading(double latitude, double longitude, float accuracy, long time){
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
 
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(GPS_LATITUDE, latitude);
        values.put(GPS_LONGITUDE, longitude);
        values.put(GPS_ACCURACY, accuracy);
        values.put(GPS_TIME, time); 
 
        // 3. insert
        db.insert(TABLE_GPS, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values
 
        // 4. close
        db.close(); 
    }
	
	public void addBluetoothReading(String id, long time, short rssi, boolean isLeType){
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
 
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(BLUETOOTH_ID, id);
        values.put(BLUETOOTH_TIME, time);
        values.put(BLUETOOTH_RSSI, rssi);
        if(isLeType)
        	values.put(BLUETOOTH_TYPE, 1);
        else
        	values.put(BLUETOOTH_TYPE, 0);
 
        // 3. insert
        db.insert(TABLE_BLUETOOTH, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values
 
        // 4. close
        db.close(); 
    }
	
	public void addCellTowerReading(int id, int lac, int mcc, int mnc, long time){
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
 
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(CELL_ID, id);
        values.put(CELL_MCC, mcc);
        values.put(CELL_MNC, mnc);
        values.put(CELL_LAC, lac); 
        values.put(CELL_TIME, time); 
 
        // 3. insert
        db.insert(TABLE_CELLTOWER, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values
 
        // 4. close
        db.close(); 
    }
	
	public void addStationRecord(String stationID, long time){
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
 
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(STATION_TIME, time);
        values.put(STATION_ID, stationID); 
 
        // 3. insert
        db.insert(TABLE_STATION, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values
 
        // 4. close
        db.close(); 
    }
	
	public void addEventRecord(String name, long time){
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
 
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(EVENT_TIME, time);
        values.put(EVENT_NAME, name); 
 
        // 3. insert
        db.insert(TABLE_EVENT, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values
 
        // 4. close
        db.close(); 
    }
	
	public List<GPSReading> getGPSReading(int limit) {
    	
    	SQLiteDatabase db = this.getWritableDatabase();
    	
        Cursor cursor = db.query(
            TABLE_GPS,
            new String[] {
                GPS_LONGITUDE,
                GPS_LATITUDE,
                GPS_ACCURACY,
                GPS_TIME
            },
            null,
            null,
            null,
            null,
            GPS_TIME + " ASC",
            String.valueOf(limit)
        );
        List<GPSReading> positions = new ArrayList<GPSReading>(limit);
        while (cursor.moveToNext()) {
            positions.add(new GPSReading(
                // These numeric indexes relate to the column order
                // defined in the query() call above.
                cursor.getDouble(0),
                cursor.getDouble(1),
                cursor.getFloat(2),
                cursor.getLong(3)
            ));
        }
 
        Log.d("getAllPositions()", positions.toString());
        
        db.close();
        return positions;
    }
	
	public JSONObject getGPSReadingJSON(int limit) throws JSONException {
    	JSONObject locationScan = new JSONObject();
        JSONArray locationArray = new JSONArray();
    	SQLiteDatabase db = this.getWritableDatabase();
    	
        Cursor cursor = db.query(
            TABLE_GPS,
            new String[] {
            	GPS_LONGITUDE,
            	GPS_LATITUDE,
            	GPS_ACCURACY,
            	GPS_TIME
            },
            null,
            null,
            null,
            null,
            GPS_TIME + " ASC",
            String.valueOf(limit)
        );
        while (cursor.moveToNext()) {
            JSONObject readings = new JSONObject();
            readings.put("ScanTime", cursor.getLong(3));
            readings.put("Longitude", cursor.getDouble(0));
            readings.put("Latitude", cursor.getDouble(1));
            readings.put("Accuracy", cursor.getFloat(2));
            locationArray.put(readings);
        }
        db.close();
        locationScan.put("GPSReadings", locationArray);
        return locationScan;
    }
	
	public void deleteGPSReadingsUntil(long time)
    {
    	SQLiteDatabase db = this.getWritableDatabase();
    	db.delete(
            TABLE_GPS,
            GPS_TIME + " <= ?",
            new String[] { String.valueOf(time) }
        );
    	db.close();
    }
	
	public List<BluetoothReading> getBluetoothReading(int limit) {
    	
    	SQLiteDatabase db = this.getWritableDatabase();
    	
        Cursor cursor = db.query(
        	TABLE_BLUETOOTH,
        	new String[] {
            		BLUETOOTH_TIME,
            		BLUETOOTH_ID,
            		BLUETOOTH_RSSI,
            		BLUETOOTH_TYPE
            },
            null,
            null,
            null,
            null,
            BLUETOOTH_TIME + " ASC",
            String.valueOf(limit)
        );
        List<BluetoothReading> bluetooths = new ArrayList<BluetoothReading>(limit);
        while (cursor.moveToNext()) {
        	bluetooths.add(new BluetoothReading(
                // These numeric indexes relate to the column order
                // defined in the query() call above.
                cursor.getLong(0),
                cursor.getString(1),
                cursor.getShort(2),
                cursor.getInt(3)
            ));
        }
        
        db.close();
        return bluetooths;
    }
	
	public JSONObject getBluetoothJSON(int limit) throws JSONException {
    	JSONObject event = new JSONObject();
        JSONArray eventArray = new JSONArray();
    	SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(
        		TABLE_BLUETOOTH,
            new String[] {
        		BLUETOOTH_TIME,
        		BLUETOOTH_ID,
        		BLUETOOTH_RSSI,
        		BLUETOOTH_TYPE
            },
            null,
            null,
            null,
            null,
            BLUETOOTH_TIME + " ASC",
            String.valueOf(limit)
        );
        while (cursor.moveToNext()) {
        	JSONObject reading = new JSONObject();
            reading.put("timestamp",cursor.getLong(0));
            reading.put("macAddress",cursor.getString(1));
            reading.put("rssi", cursor.getShort(2));
            if(cursor.getInt(3)==1)
            	reading.put("isLEType", "true");
            else
            	reading.put("isLEType", "false");
            eventArray.put(reading);
        }
        db.close();
        event.put("Bluetooth",eventArray);
        return event;
    }
	
	public void deleteBluetoothUntil(long time)
    {
    	SQLiteDatabase db = this.getWritableDatabase();
        db.delete(
        	TABLE_BLUETOOTH,
        	BLUETOOTH_TIME + " <= ?",
            new String[] { String.valueOf(time) }
        );
        db.close();
    }

	public List<CellTowerReading> getCellReading(int limit) {
    	
    	SQLiteDatabase db = this.getWritableDatabase();
    	
        Cursor cursor = db.query(
            TABLE_CELLTOWER,
            new String[] {
                CELL_ID,
                CELL_MCC,
                CELL_MNC,
                CELL_LAC,
                CELL_TIME
            },
            null,
            null,
            null,
            null,
            CELL_TIME + " ASC",
            String.valueOf(limit)
        );
        List<CellTowerReading> positions = new ArrayList<CellTowerReading>(limit);
        while (cursor.moveToNext()) {
            positions.add(new CellTowerReading(
                // These numeric indexes relate to the column order
                // defined in the query() call above.
            		cursor.getInt(0),
            		cursor.getInt(1),
            		cursor.getInt(2),
            		cursor.getInt(3),
            		cursor.getLong(4)
            ));
        }
        
        db.close();
        return positions;
    }
	
	public JSONObject getCellReadingJSON(int limit) throws JSONException {
    	JSONObject locationScan = new JSONObject();
        JSONArray locationArray = new JSONArray();
    	SQLiteDatabase db = this.getWritableDatabase();
    	
        Cursor cursor = db.query(
            TABLE_CELLTOWER,
            new String[] {
            	CELL_ID,
            	CELL_MCC,
            	CELL_MNC,
            	CELL_LAC,
            	CELL_TIME
            },
            null,
            null,
            null,
            null,
            CELL_TIME + " ASC",
            String.valueOf(limit)
        );
        while (cursor.moveToNext()) {
            JSONObject readings = new JSONObject();
            readings.put("ScanTime", cursor.getLong(4));
            readings.put("ID", cursor.getInt(0));
            readings.put("LAC", cursor.getInt(1));
            readings.put("MCC", cursor.getInt(2));
            readings.put("MNC", cursor.getInt(3));
            locationArray.put(readings);
        }
        db.close();
        locationScan.put("CellTowerReadings", locationArray);
        return locationScan;
    }
	
	public void deleteCellReadingsUntil(long time)
    {
    	SQLiteDatabase db = this.getWritableDatabase();
    	db.delete(
            TABLE_CELLTOWER,
            CELL_TIME + " <= ?",
            new String[] { String.valueOf(time) }
        );
    	db.close();
    }
	
	public List<StationRecord> getStationRecord(int limit) {
    	
    	SQLiteDatabase db = this.getWritableDatabase();
    	
        Cursor cursor = db.query(
            TABLE_STATION,
            new String[] {
                STATION_TIME,
                STATION_ID,
            },
            null,
            null,
            null,
            null,
            STATION_TIME + " ASC",
            String.valueOf(limit)
        );
        List<StationRecord> positions = new ArrayList<StationRecord>(limit);
        while (cursor.moveToNext()) {
            positions.add(new StationRecord(
                // These numeric indexes relate to the column order
                // defined in the query() call above.
            		cursor.getString(1),
            		cursor.getLong(0)
            ));
        }
        
        db.close();
        return positions;
    }
	
	public JSONObject getStationRecordJSON(int limit) throws JSONException {
    	JSONObject locationScan = new JSONObject();
        JSONArray locationArray = new JSONArray();
    	SQLiteDatabase db = this.getWritableDatabase();
    	
        Cursor cursor = db.query(
        		TABLE_STATION,
                new String[] {
                    STATION_TIME,
                    STATION_ID,
                },
            null,
            null,
            null,
            null,
            STATION_TIME + " ASC",
            String.valueOf(limit)
        );
        while (cursor.moveToNext()) {
            JSONObject readings = new JSONObject();
            readings.put("time", cursor.getLong(0));
            readings.put("StationID", cursor.getInt(1));
            locationArray.put(readings);
        }
        db.close();
        locationScan.put("StationRecord", locationArray);
        return locationScan;
    }
	
	public void deleteStationRecordsUntil(long time)
    {
    	SQLiteDatabase db = this.getWritableDatabase();
    	db.delete(
            TABLE_STATION,
            STATION_TIME + " <= ?",
            new String[] { String.valueOf(time) }
        );
    	db.close();
    }
	
	public List<EventRecord> getEventRecord(int limit) {
    	
    	SQLiteDatabase db = this.getWritableDatabase();
    	
        Cursor cursor = db.query(
            TABLE_EVENT,
            new String[] {
                EVENT_TIME,
                EVENT_NAME,
            },
            null,
            null,
            null,
            null,
            EVENT_TIME + " ASC",
            String.valueOf(limit)
        );
        List<EventRecord> events = new ArrayList<EventRecord>(limit);
        while (cursor.moveToNext()) {
        	events.add(new EventRecord(
                // These numeric indexes relate to the column order
                // defined in the query() call above.
            		cursor.getString(1),
            		cursor.getLong(0)
            ));
        }
        
        db.close();
        return events;
    }
	
	public JSONObject getEventRecordJSON(int limit) throws JSONException {
        JSONObject event = new JSONObject();
        JSONArray eventArray = new JSONArray();
    	SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.query(
        		TABLE_EVENT,
                new String[] {
                    EVENT_TIME,
                    EVENT_NAME,
                },
            null,
            null,
            null,
            null,
            EVENT_TIME + " ASC",
            String.valueOf(limit)
        );
        while (cursor.moveToNext()) {
            JSONObject reading = new JSONObject();
            reading.put("timestamp",cursor.getLong(0));
            reading.put("label",cursor.getString(1));
            eventArray.put(reading);
        }
        db.close();
        event.put("Event",eventArray);
        return event;
    }
	
	public void deleteEventRecordsUntil(long time)
    {
    	SQLiteDatabase db = this.getWritableDatabase();
    	db.delete(
            TABLE_EVENT,
            EVENT_TIME + " <= ?",
            new String[] { String.valueOf(time) }
        );
    	db.close();
    }

}
