package au.gov.vic.ptv.android.crowdwatcher.model.DataSubmission;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import au.gov.vic.ptv.android.crowdwatcher.model.SensorsDatabase;

/**
 * This class will be use to run IntentService in Application Background to post sensor data
 * to web server. The data will be converted to JSON and sent to web server. Data will only be
 * sent to web server if device is connected to wifi.
 *
 * The service utilizing this class to send data should be trigger every X minute interval using
 * AlarmManager.
 */
public class DataSubmissionIntentService extends IntentService {

    private SensorsDatabase db;
    private String deviceKey;
    private DataSubmitter dataSubmitter;

    public DataSubmissionIntentService(){
        super("DataSubmissionIntentService");
    }

    @Override
    public void onCreate() {
        Log.d("DataSubmission", "onCreate");

        // TODO Auto-generated method stub
        super.onCreate();
        db = new SensorsDatabase(this);
        getDeviceKey();
        dataSubmitter = new DataSubmitter(db);
        showDbSize();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        long serviceStopTime = System.currentTimeMillis()+(1*25*1000);

        // continuously post data to web server up to 30 seconds
        while(serviceStopTime > System.currentTimeMillis()){
            if (this.isConnectedWifi()) {
                dataSubmitter.postData(deviceKey);
            }else{
                break;
            }
        }

        Log.d("DataSubmission", "onStartCommand ends");
    }

    private void getDeviceKey(){
        // get device key from sharedPreference
        SharedPreferences pref;
        pref = getSharedPreferences("au.gov.vic.ptv.android.crowdwatcher", MODE_MULTI_PROCESS);
        deviceKey = pref.getString("au.gov.vic.ptv.android.crowdwatcher.key", null);

        // request new device key if app is newly installed
        if(deviceKey == null){
            try {
                // waits up to 10 seconds for http response
                deviceKey = new WebServiceGetKeyAsyncTask().execute().get(10000, TimeUnit.MILLISECONDS);
                Log.d("DataSubmission","key retrieved"+deviceKey);
                // saves device key
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("au.gov.vic.ptv.android.crowdwatcher.key", deviceKey);
                editor.commit();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ExecutionException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (TimeoutException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("NewApi")
    private void showDbSize(){
        long dbSizeByte = this.getDatabasePath(db.getDatabaseName()).length();
        double dbSizeMb = dbSizeByte/(double)1048576L;
        Toast.makeText(this, "Crowdsensing App Database size : " + dbSizeMb,
                Toast.LENGTH_LONG).show();
    }

    /**
    * Checks if device is connected to wifi network
    *
    */
    public boolean isConnectedWifi() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_WIFI && networkInfo.isConnected())
            return true;
        else
            return false;
    }
}
