package au.gov.vic.ptv.android.crowdwatcher.model;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import android.util.Log;

public class PTVAPIsignature {
	
	private String privateKey;
	private String developerID;
	private static PTVAPIsignature instance = null;
	
	/*http://androidexample.com/Restful_Webservice_Call_And_Get_And_Parse_JSON_Data-_Android_Example/index.php?view=article_discription&aid=101&aaid=123
	*/
	
	private String healthCheckURL;
	/*
	 * Parameters:
	 * timestamp	=	optional: the date and time of the request in ISO 8601 UTC format
	 * devid	=	optional: the developer ID supplied in your email from PTV
	 * signature	=	optional:
	 * 
	 * Response output:
	 * {
	 *   "securityTokenOK": boolean,
	 *   "clientClockOK": boolean,
	 *   "memcacheOK": boolean,
	 *   "databaseOK": boolean,
	 * }
     */
	
	private String stopsNearbyURL;
	/*
	 * Parameters
	 * latitude	=	prescribed latitude, expressed in decimal degrees.
	 * longitude	=	prescribed longitude, expressed in decimal degrees.
	 * devid	=	the developer ID supplied in your email from PTV
	 * signature	=	the customised message digest calculated using the method in the 
	 * 
	 * Response output:
	 * {
	 *   suburb	(string) = the suburb name
	 *   transport_type	(string) = the mode of transport serviced by the stop
	 *   stop_id (numeric string) = the unique identifier of each stop
	 *   location_name	(string) = the name of the stop based on a concise geographic description
	 *   lat (decimal number) = geographic coordinate of latitude
	 *   lon (decimal number) = geographic coordinate of longitude
	 *   distance (decimal number) = not used in the context of this API (it is a legacy attribute of unknown worth)
	 * }
	 */
	
	private PTVAPIsignature(){
		this.developerID = "1000260";
		this.privateKey = "50a53356-2e85-11e4-8bed-0263a9d0b8a0";
		this.healthCheckURL = "/v2/HealthCheck";
		this.stopsNearbyURL = "/v2/nearme/";
	}
	
	public static PTVAPIsignature getInstance(){
		if(instance==null){
			instance = new PTVAPIsignature();
		}
		
		return instance;
	}
	
	public String getPrivateKey(){
		return this.privateKey;
	}
	
	public String getDevID(){
		return this.developerID;
	}
	
	public String getHealthCheckURL() {
		return healthCheckURL;
	}

	public String getStopsNearbyURL() {
		return stopsNearbyURL;
	}
	
	public String generateStopsNearbyURL(double latitude, double longitude){
		StringBuffer buffer = new StringBuffer().append("/v2/nearme/");
		buffer.append("latitude/").append(latitude).append("/longitude/").append(longitude);
		return buffer.toString();
	}
	
	/**
	 * Generates a signature using the HMAC-SHA1 algorithm 
	 * 
	 * @param privateKey - Developer Key supplied by PTV
	 * @param uri - request uri (Example :/v2/HealthCheck) 
	 * @param developerId - Developer ID supplied by PTV
	 * @return Unique Signature Value  
	 */
	public String generateSignature(final String privateKey, final String uri, final int developerId)
	{
	    String encoding = "UTF-8";
	    String HMAC_SHA1_ALGORITHM = "HmacSHA1";
	    String signature;
	    StringBuffer uriWithDeveloperID = new StringBuffer();
	    uriWithDeveloperID.append(uri).append(uri.contains("?") ? "&" : "?").append("devid="+developerId);     
	    try
	    {
	        byte[] keyBytes = privateKey.getBytes(encoding);
	        byte[] uriBytes = uriWithDeveloperID.toString().getBytes(encoding);
	        Key signingKey = new SecretKeySpec(keyBytes, HMAC_SHA1_ALGORITHM);
	        Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
	        mac.init(signingKey);
	        byte[] signatureBytes = mac.doFinal(uriBytes);
	        StringBuffer buf = new StringBuffer(signatureBytes.length * 2);
	        for (byte signatureByte : signatureBytes)
	        {
	            int intVal = signatureByte & 0xff;
	            if (intVal < 0x10)
	            {
	                buf.append("0");
	            }
	            buf.append(Integer.toHexString(intVal));
	        }
	        signature = buf.toString();
	    }
	    catch (UnsupportedEncodingException e)
	    {
	        throw new RuntimeException(e);
	    }
	    catch (NoSuchAlgorithmException e)
	    {
	        throw new RuntimeException(e);
	    }
	    catch (InvalidKeyException e)
	    {
	        throw new RuntimeException(e);
	    }
	    return signature.toString().toUpperCase();
	}

	/**
	 * Generate full URL using generateSignature() method
	 * 
	 * @param privateKey - Developer Key supplied by PTV (Example :  "92dknhh31-6a30-4cac-8d8b-8a1970834799");
	 * @param uri - request uri (Example :"/v2/mode/2/line/787/stops-for-line) 
	 * @param developerId - Developer ID supplied by PTV( int developerId )
	 * @return - Full URL with Signature
	 */
	public String generateCompleteURLWithSignature(final String privateKey, final String uri, final int developerId)
	{

	    String baseURL="http://timetableapi.ptv.vic.gov.au";
	    StringBuffer url = new StringBuffer(baseURL).append(uri).append(uri.contains("?") ? "&" : "?").append("devid="+developerId).append("&signature="+generateSignature(privateKey, uri, developerId));
	    return url.toString();

	}


}
