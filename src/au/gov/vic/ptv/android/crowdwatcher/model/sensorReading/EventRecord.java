package au.gov.vic.ptv.android.crowdwatcher.model.sensorReading;

public class EventRecord {
	
	private String name;
	private long time;
	
	public EventRecord(String name, long time){
		this.name = name;
		this.time = time;
	}
	
	public String getEvent() {
		return name;
	}

	public long getTime() {
		return time;
	}


}
