package au.gov.vic.ptv.android.crowdwatcher.model.sensorReading;

public class GPSReading {
	private double longitude;
    private double latitude;
    private float accuracy;
    private long time;

    /**
     * Construct a new {@code GPSReading} object.
     *
     * @param longitude in degrees.
     * @param latitude in degrees.
     * @param accuracy in metres.
     * @param time in UTC milliseconds since 1 January 1970.
     */
    public GPSReading(
        double longitude, double latitude, float accuracy, long time
    ) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.accuracy = accuracy;
        this.time = time;
    }

    /**
     * A human-readable description of this object.
     *
     * Used solely for debugging purposes.
     */
    public String toString() {
        return "Position [longitude=" + longitude + ", latitude=" + latitude
            + ", accuracy=" + accuracy + ", time=" + time + "]";
    }

    /**
     * The longitude of this reading, in degrees.
     */
    public double getLongitude() {
        return this.longitude;
    }

    /**
     * The latitude of this reading, in degrees.
     */
    public double getLatitude() {
        return this.latitude;
    }

    /**
     * The estimated accuracy of this reading, in metres.
     */
    public float getAccuracy() {
        return this.accuracy;
    }

    /**
     * The UTC time of this reading, in milliseconds since 1 January
     * 1970.
     */
    public long getTime() {
        return this.time;
    }
}
