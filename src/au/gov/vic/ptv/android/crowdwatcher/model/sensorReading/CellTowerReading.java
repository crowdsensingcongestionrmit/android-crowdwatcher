package au.gov.vic.ptv.android.crowdwatcher.model.sensorReading;

public class CellTowerReading {
	
	private long time;
	private int id;
	private int lac;
	private int mcc;
	private int mnc;
	
	public CellTowerReading(int id, int lac, int mcc, int mnc, long time){
		this.id = id;
		this.lac = lac;
		this.mcc = mcc;
		this.mnc = mnc;
		this.time = time;
	}
	
	public long getTime() {
		return time;
	}

	public int getId() {
		return id;
	}

	public int getLac() {
		return lac;
	}

	public int getMcc() {
		return mcc;
	}

	public int getMnc() {
		return mnc;
	}


}
