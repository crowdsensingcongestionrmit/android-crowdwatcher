package au.gov.vic.ptv.android.crowdwatcher.model.sensorReading;

public class StationRecord {
	
	private String stationID;
	private long time;
	
	public StationRecord(String stationID, long time){
		this.stationID = stationID;
		this.time = time;
	}
	
	public String getStationID() {
		return stationID;
	}

	public long getTime() {
		return time;
	}


}
