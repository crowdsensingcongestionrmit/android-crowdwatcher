package au.gov.vic.ptv.android.crowdwatcher.model.dataCollection;


import android.location.Location;
import android.os.AsyncTask;

import org.apache.http.HttpStatus;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import au.gov.vic.ptv.android.crowdwatcher.JSONParser.JSONParser;
import au.gov.vic.ptv.android.crowdwatcher.R;
import au.gov.vic.ptv.android.crowdwatcher.model.calculator.ConfidenceLevelCalculator;
import au.gov.vic.ptv.android.crowdwatcher.model.PTVAPIsignature;

/**
 * This is used to send webservice call to ptv api to retrieve nearbyStops from the provided
 * location
 */
public class GetNearbyStopServiceAsyncTask extends AsyncTask<String, Void, ArrayList<HashMap<String,String>>> {

    private LocationService service;
    private Location location;

    private final int stopsDistancetreshold = 500;
    private int stopRadius = 10;

    public GetNearbyStopServiceAsyncTask(LocationService service, Location location) {
        this.service = service;
        this.location = location;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }


    @Override
    protected ArrayList<HashMap<String, String>> doInBackground(String... params) {

        ArrayList<HashMap<String,String>> nearbyStops;
        JSONParser jsonParser = new JSONParser(service.getApplicationContext());
        String APIResponse =requestPTVAPI(location.getLatitude(), location.getLongitude());
        try {
        	nearbyStops = jsonParser.parseNearByStation(APIResponse);
        } catch (JSONException e) {
            e.printStackTrace();
            nearbyStops =  new ArrayList<HashMap<String, String>>();
        }

        nearbyStops = filterList(nearbyStops);
        ArrayList<HashMap<String, String>>filterStops = filterList(nearbyStops);
        return filterStops;
    }

    @Override
    protected void onPostExecute(ArrayList<HashMap<String, String>> nearbyStops) {
//        ArrayList<HashMap<String, String>>filterStops = filterList(nearbyStops);
    }

    private ArrayList<HashMap<String, String>> filterList(
			ArrayList<HashMap<String, String>> nearbyStops) {
		// TODO Auto-generated method stub
    	ConfidenceLevelCalculator calculator = new ConfidenceLevelCalculator();
    	/* (1)filter the nearbyStops, create a new collection(eg ArrayList),
    	 * 	  that has all stops distance less than stopsDistancetreshold(It is instantiate at the top).*/

        ArrayList<HashMap<String,String>> nearbyStopWithinTreshold = new ArrayList<HashMap<String, String>>();

        for(HashMap<String,String> stop : nearbyStops){
           double distance = Double.parseDouble(stop.get(this.service.getApplicationContext().getString(R.string.nearby_stop_distance_tag)));
            if(distance < stopsDistancetreshold){
                 /* (2)for all stops in the new collection, call the calculateOverlapArea in calculator, and add the return value
                 *    into the stops detail(the method return the percentage area of overlap of two circle)
                 *    this method receives(GPS Accuracy, stopRadius, stop distance)
                 *    stopRadius is defined at top.*/
                String a = service.getApplicationContext().getString(R.string.nearby_stop_transport_type_tag);
                int b = stop.get(a).compareTo("tram");
                if(stop.get(service.getApplicationContext().getString(R.string.nearby_stop_transport_type_tag)).compareTo("tram")==0){
                    stopRadius = 50;
                }
                if(stop.get(service.getApplicationContext().getString(R.string.nearby_stop_transport_type_tag)).compareTo("train")==0){
                    stopRadius = 150;
                }


                double confidenceLevel = calculator.calculateConfidenceLevel(location.getAccuracy(),stopRadius,distance);
                stop.put(this.service.getApplicationContext().getString(R.string.nearby_stop_confidence_level_tag),String.valueOf(confidenceLevel));
                nearbyStopWithinTreshold.add(stop);
            }
        }
        /* (3)return the processed collection */
		return nearbyStopWithinTreshold;
	}

	private String requestPTVAPI(double lat, double longi) {
    	// TODO Auto-generated method stub
    	String responseString ="";
    	HttpURLConnection urlConnection = null;
    	PTVAPIsignature instance = PTVAPIsignature.getInstance();
    	String temp = instance.generateStopsNearbyURL(lat, longi);
    	String url = instance.generateCompleteURLWithSignature(instance.getPrivateKey(), temp, Integer.parseInt(instance.getDevID()));
    	try 
    	{  

    		// Defined URL  where to send data 
    		URL urlWithSignature = new URL(url); 

    		urlConnection = (HttpURLConnection) urlWithSignature.openConnection();
    		int responseCode = urlConnection.getResponseCode();

    		if(responseCode == HttpStatus.SC_OK){
    			responseString = readStream(urlConnection.getInputStream());
    		}

    	} catch (Exception e) {
    		e.printStackTrace();
    	} finally {
    		if(urlConnection != null)
    			urlConnection.disconnect();
    	}

    	return responseString;
    }
    
    private String readStream(InputStream in) {
		  BufferedReader reader = null;
		  StringBuffer response = new StringBuffer();
		  try {
		    reader = new BufferedReader(new InputStreamReader(in));
		    String line = "";
		    while ((line = reader.readLine()) != null) {
		      response.append(line);
		    }
		  } catch (IOException e) {
		    e.printStackTrace();
		  } finally {
		    if (reader != null) {
		      try {
		        reader.close();
		      } catch (IOException e) {
		        e.printStackTrace();
		      }
		    }
		  }
		  return response.toString();
		}
}
