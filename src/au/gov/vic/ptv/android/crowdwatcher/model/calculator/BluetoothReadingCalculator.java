package au.gov.vic.ptv.android.crowdwatcher.model.calculator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.gov.vic.ptv.android.crowdwatcher.model.sensorReading.BluetoothReading;

public class BluetoothReadingCalculator {
	private Map<String, Integer> map;
	
	public BluetoothReadingCalculator(){
	}
	
	public void saveLatestReading(List<BluetoothReading> readings){
		map = new HashMap<String, Integer>();
		for(BluetoothReading reading: readings){
			map.put(reading.getID(), 0);
		}
	}
	
	public double calculateConfidenceLevel(List<BluetoothReading> readings){
		double count=0;
		restartCount();
		for(BluetoothReading reading: readings){
			if(map.containsKey(reading.getID()))
				map.put(reading.getID(), map.get(reading.getID())+1);
		}
		for(Integer value: map.values()){
			count = count+value;
		}
		
		return (map.size()/count*100);
	}
	
	private void restartCount(){
		for(Map.Entry<String, Integer> entry: map.entrySet()){
			map.put(entry.getKey(), 0);
		}
	}
	
	public List<String> findDifference(List<BluetoothReading> readings){
		List<String> differentReading = new ArrayList<String>();
		for(BluetoothReading reading: readings){
			if(!map.containsKey(reading.getID()))
				differentReading.add(reading.getID());
		}
		return differentReading;
	}

}
