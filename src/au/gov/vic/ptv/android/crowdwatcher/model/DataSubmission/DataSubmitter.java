package au.gov.vic.ptv.android.crowdwatcher.model.DataSubmission;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import au.gov.vic.ptv.android.crowdwatcher.model.SensorsDatabase;

/**
 * This class acts as a util class for DataSubmissionIntentService to send data to web server.
 * The actual code for sending data is in this class.
 *
 * Uses aynctask to send data through http post. Each time it will retrieve 50 rows from each sensor
 * data to be submitted to web server.
 */
public class DataSubmitter {

    private final int retriveRowNo = 50;
    private SensorsDatabase db;

    /**
     * Constructs an object is used in DataSubmissionIntentService to submit date to web server.
     * @param db the Database object which needs to be use to retrieve and send data
     */
    public DataSubmitter(SensorsDatabase db){
        this.db = db;
    }

    /**
     * This method is used by different method for sending sensor data to webserver
     *
     * @param deviceKey unique key for different device which should be saved in shared preference
     * @param jsonObject the sensor data which is transformed into JSON format
     * @return The true if the data is sent to the web server successfully
     * @throws InterruptedException When the asynctask is interrupted
     * @throws ExecutionException  When executing asynctask
     * @throws JSONException When JSON could not be built
     */
    public boolean sendData(String deviceKey, JSONObject jsonObject) throws InterruptedException, ExecutionException, JSONException {
        return new WebServiceStoreAsyncTask().execute(deviceKey,jsonObject).get();

    }

    /**
     * Uses different method (e.g. sendGPS()) for posting different sensor data to web server.
     * This is used by DataSubmissionIntentService to post all sensor data to web server.
     *
     * @param deviceKey unique key for different device which should be saved in shared preference
     */
    public void postData(String deviceKey){
        Log.d("postData",deviceKey);
        try {
            sendGPS(deviceKey);
            sendBluetoothDevice(deviceKey);
            sendCellTower(deviceKey);
            sendStation(deviceKey);
            sendLabel(deviceKey);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

    }

    /**
     * This method is to be used in postData method to retrieve GPS data as JSON object and
     * delete from database if the data is submitted to web server successfully. Uses sendData
     * method to trigger asynctask to send data to web server.
     *
     * @param deviceKey unique key for different device to uniquely identify the the device
     * @throws InterruptedException When the asynctask is interrupted
     * @throws ExecutionException  When executing asynctask
     * @throws JSONException When JSON could not be built
     */
    private void sendGPS(String deviceKey) throws JSONException, InterruptedException, ExecutionException, TimeoutException {
        JSONObject gpsJSON = db.getGPSReadingJSON(retriveRowNo);

        boolean gpsSuccess = false;

        JSONArray gspJSONArray = gpsJSON.getJSONArray("GPSReadings");

        if(gspJSONArray.length() == 0)
            return;
        long gpsMaxTime = gspJSONArray.getJSONObject(gspJSONArray.length()-1).getLong("ScanTime");


        gpsSuccess = sendData(deviceKey,gpsJSON);
        Log.d("DataSubmission", "gps post status : " + gpsSuccess);
        if(gpsSuccess){
            db.deleteGPSReadingsUntil(gpsMaxTime);
        }

    }

    /**
     * This method is to be used in postData method to retrieve Bluetooth data as JSON object and
     * delete from database if the data is submitted to web server successfully. Uses sendData
     * method to trigger asynctask to send data to web server.
     *
     * @param deviceKey unique key for different device to uniquely identify the the device
     * @throws InterruptedException When the asynctask is interrupted
     * @throws ExecutionException  When executing asynctask
     * @throws JSONException When JSON could not be built
     */
    private void sendBluetoothDevice(String deviceKey) throws JSONException, InterruptedException, ExecutionException, TimeoutException {
        JSONObject bluetoothJSON = db.getBluetoothJSON(retriveRowNo);

        boolean bluetoothSuccess = false;

        JSONArray bluetoothJSONJSONArray = bluetoothJSON.getJSONArray("Bluetooth");

        if(bluetoothJSONJSONArray.length() == 0)
            return;
        long bluetoothMaxTime = bluetoothJSONJSONArray.getJSONObject(bluetoothJSONJSONArray.length()-1).getLong("timestamp");

        bluetoothSuccess = sendData(deviceKey,bluetoothJSON);
        Log.d("DataSubmission", "BluetoothDevice post status : " + bluetoothSuccess);
        if(bluetoothSuccess){
            db.deleteBluetoothUntil(bluetoothMaxTime);
        }

    }

    /**
     * This method is to be used in postData method to retrieve CellTower data as JSON object and
     * delete from database if the data is submitted to web server successfully. Uses sendData
     * method to trigger asynctask to send data to web server.
     *
     * @param deviceKey unique key for different device to uniquely identify the the device
     * @throws InterruptedException When the asynctask is interrupted
     * @throws ExecutionException  When executing asynctask
     * @throws JSONException When JSON could not be built
     */
    private void sendCellTower(String deviceKey) throws JSONException, InterruptedException, ExecutionException, TimeoutException {
        JSONObject cellReadingJSON = db.getCellReadingJSON(retriveRowNo);

        boolean cellReadingSuccess = false;

        JSONArray cellReadingJSONArray = cellReadingJSON.getJSONArray("CellTowerReadings");

        if(cellReadingJSONArray.length() == 0)
            return;
        long cellReadingMaxTime = cellReadingJSONArray.getJSONObject(cellReadingJSONArray.length()-1).getLong("ScanTime");

        cellReadingSuccess = sendData(deviceKey,cellReadingJSON);
        Log.d("DataSubmission", "CellReadings post status : " + cellReadingSuccess);
        if(cellReadingSuccess){
            db.deleteCellReadingsUntil(cellReadingMaxTime);
        }

    }

    /**
     * This method is to be used in postData method to retrieve Station data as JSON object and
     * delete from database if the data is submitted to web server successfully. Uses sendData
     * method to trigger asynctask to send data to web server.
     *
     * @param deviceKey unique key for different device to uniquely identify the the device
     * @throws InterruptedException When the asynctask is interrupted
     * @throws ExecutionException  When executing asynctask
     * @throws JSONException When JSON could not be built
     */
    private void sendStation(String deviceKey) throws JSONException, InterruptedException, ExecutionException, TimeoutException {
        JSONObject stationsJSON = db.getStationRecordJSON(retriveRowNo);

        boolean stationsSuccess = false;

        JSONArray stationJSONArray = stationsJSON.getJSONArray("StationRecord");
        if(stationJSONArray.length() == 0)
            return;
        long stationRecordMaxTime = stationJSONArray.getJSONObject(stationJSONArray.length()-1).getLong("time");

        stationsSuccess = sendData(deviceKey,stationsJSON);
        Log.d("DataSubmission", "StationRecord post status : " + stationsSuccess);
        if(stationsSuccess){
            db.deleteStationRecordsUntil(stationRecordMaxTime);
        }

    }

    /**
     * This method is to be used in postData method to retrieve Label data as JSON object and
     * delete from database if the data is submitted to web server successfully. Uses sendData
     * method to trigger asynctask to send data to web server.
     *
     * @param deviceKey unique key for different device to uniquely identify the the device
     * @throws InterruptedException When the asynctask is interrupted
     * @throws ExecutionException  When executing asynctask
     * @throws JSONException When JSON could not be built
     */
    private void sendLabel(String deviceKey) throws JSONException, InterruptedException, ExecutionException {
        JSONObject labelJSON = db.getEventRecordJSON(retriveRowNo);
        boolean labelSuccess = false;

        JSONArray array = labelJSON.getJSONArray("Event");

        if(array.length() == 0)
            return;

        JSONObject lastElement = array.getJSONObject(array.length()-1);
        long endTime = lastElement.getLong("timestamp");

        labelSuccess = sendData(deviceKey,labelJSON);
        Log.d("DataSubmitter", "sendLabel post status : " + labelSuccess);
        if(labelSuccess){
            db.deleteEventRecordsUntil(endTime);
        }

    }
}
