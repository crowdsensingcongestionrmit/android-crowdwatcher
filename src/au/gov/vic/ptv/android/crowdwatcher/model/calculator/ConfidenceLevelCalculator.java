package au.gov.vic.ptv.android.crowdwatcher.model.calculator;

import android.util.Log;

/**
 * This class provides method to calculate confidence level to determine if the device is in a
 * station using GPS radius and the radius of the station. Confidence Level 0 - 100%.
 */
public class ConfidenceLevelCalculator {
	
	public double calculateConfidenceLevel(double GPSAccuracy, double stopsRadius, double distance){
		if(checkInside(GPSAccuracy, stopsRadius, distance))
			return 100.00;
		if(noOverlap(GPSAccuracy, stopsRadius, distance))
			return 0.0;
		
		double confidenceLevel;
		
		Double r = GPSAccuracy;
		Double R = stopsRadius;
		Double d = distance;
		Double area = r*r*Math.PI;
		
		Log.i("GPSAccuracy",""+GPSAccuracy);
		Log.i("stopsRadius",""+stopsRadius);
		Log.i("distance",""+distance);
		if(R < r){
		    // swap
		    r = stopsRadius;
		    R = GPSAccuracy;
		}
		Double part1 = r*r*Math.acos((d*d + r*r - R*R)/(2*d*r));
		Double part2 = R*R*Math.acos((d*d + R*R - r*r)/(2*d*R));
		Double part3 = 0.5*Math.sqrt((-d+r+R)*(d+r-R)*(d-r+R)*(d+r+R));

		confidenceLevel = (part1 + part2 - part3)/area*100;
		Log.i("confidenceLevel",""+confidenceLevel);
		return confidenceLevel;

	}

	private boolean noOverlap(double GPSAccuracy, double stopsRadius,
			double distance) {
		// TODO Auto-generated method stub
		if(distance>GPSAccuracy+stopsRadius)
			return true;
		return false;
	}

	private boolean checkInside(double GPSAccuracy, double stopsRadius,
			double distance) {
		// TODO Auto-generated method stub
		if(GPSAccuracy+distance < stopsRadius)
			return true;
		return false;
	}

}
