package au.gov.vic.ptv.android.crowdwatcher.model.dataCollection;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

import au.gov.vic.ptv.android.crowdwatcher.broadcastReceiver.LocationDisabledBroadcastReceiver;
import au.gov.vic.ptv.android.crowdwatcher.broadcastReceiver.LocationUpdateBroadcastReceiver;
import au.gov.vic.ptv.android.crowdwatcher.model.SensorsDatabase;
import au.gov.vic.ptv.android.crowdwatcher.sensorcollection.pushsensor.BluetoothSensor;
import au.gov.vic.ptv.android.crowdwatcher.sensorcollection.pushsensor.CellTowerSensor;
import au.gov.vic.ptv.android.crowdwatcher.sensorcollection.pushsensor.LocationSensor;

/**
 * This service is used to run in background to poll for location data and cell tower data.
 */
public class LocationService extends Service{

    private LocationSensor locationSensor;
    private BluetoothSensor bluetoothSensor;
    private CellTowerSensor cellTowerSensor;
    private SensorsDatabase sensorsDatabase;
    private boolean bluetoothEnabled = false;

    @Override
    public void onCreate() {

        super.onCreate();
        sensorsDatabase = new SensorsDatabase(this);

        BroadcastReceiver locationUpdateReceiver = new LocationUpdateBroadcastReceiver(this,sensorsDatabase);
        BroadcastReceiver locationDisabledReceiver = new LocationDisabledBroadcastReceiver(this);

        LocalBroadcastManager.getInstance(this).registerReceiver(locationUpdateReceiver, new IntentFilter("au.gov.vic.ptv.android.crowdwatcher.broadcastReceiver.LocationUpdateBroadcastReceiver.update"));
        LocalBroadcastManager.getInstance(this).registerReceiver(locationDisabledReceiver, new IntentFilter("au.gov.vic.ptv.android.crowdwatcher.broadcastReceiver.LocationUpdateBroadcastReceiver.locationDisable"));

        locationSensor = new LocationSensor(this,sensorsDatabase);
        locationSensor.register();

        cellTowerSensor = new CellTowerSensor(this,sensorsDatabase);
        cellTowerSensor.register();
    }

    /**
     * Stops all sensor update when service is destroyed
     */
    @Override
    public void onDestroy() {
        locationSensor.unregister();
        bluetoothSensor.unregister();
        cellTowerSensor.unregister();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Turn on bluetooth and collect bluetooth data
     */
    public void enableBluetooth(){
        bluetoothSensor = new BluetoothSensor(this,sensorsDatabase);
        bluetoothSensor.register();
        bluetoothEnabled = true;
    }

    /**
     * Turn off bluetooth
     */
    public void disableBluetooth(){
        if(bluetoothSensor != null){
            bluetoothSensor.unregister();
            bluetoothSensor = null;
        }
        bluetoothEnabled = false;
    }

    /**
     * Checks if Bluetooth is enabled
     *
     * return true if Bluetooth is enabled
     */
    public boolean isBluetoothEnabled(){
        return bluetoothEnabled;
    }
}
